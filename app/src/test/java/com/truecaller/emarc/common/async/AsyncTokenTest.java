package com.truecaller.emarc.common.async;

import com.truecaller.emarc.BuildConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Test for {@link Future} token
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
@Config(sdk = 21, constants = BuildConfig.class)
@RunWith(RobolectricGradleTestRunner.class)
public class AsyncTokenTest {

    @Test
    public void tokenTest() {
        Future token = Async.none();
        assertThat("expect finished", token.isDone(), is(true));
        assertThat("expect cancelled", token.isCancelled(), is(false));
    }
}