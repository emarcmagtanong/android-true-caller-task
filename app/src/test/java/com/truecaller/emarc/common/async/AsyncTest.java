package com.truecaller.emarc.common.async;

import com.truecaller.emarc.BuildConfig;
import com.truecaller.emarc.common.callback.ErrorListener;
import com.truecaller.emarc.common.callback.ResponseListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;

/**
 * Test for {@link Async}
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
@Config(sdk = 21, constants = BuildConfig.class)
@RunWith(RobolectricGradleTestRunner.class)
public class AsyncTest {
    @Before
    public void setup() {
        Async.DISABLE_UI_THREAD_CHECK = true;
        Async.DISABLE_EXECUTOR_DELEGATION = false;
    }

    @Test
    public void isUiThread_onMainThread_shouldReturnTrue() throws Exception {
        assertThat("expect UI thread", Async.isUiThread(), is(true));
    }

    @Test
    public void isUiThread_onWorkerThread_shouldReturnFalse() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        // create worker thread that executes iUiThread
        class LocalThread extends Thread {
            boolean isUiThread;

            @Override
            public void run() {
                isUiThread = Async.isUiThread();
                latch.countDown();
            }
        }

        LocalThread thread = new LocalThread();
        thread.start();
        latch.await();

        assertThat("expect non-UI thread", thread.isUiThread, is(false));
    }

    @Test
    public void testUnwrap() throws Exception {
        Exception actual = new Exception();
        Exception wrapper = new ExecutionException(actual);
        assertThat("expect actual", Async.unwrap(actual), is(actual));
        assertThat("expect actual", Async.unwrap(wrapper), is(actual));
    }

    @Test
    public void schedule_returnResult_expectResult() throws Exception {
        ResultListener<String, Exception> listener = new ResultListener<>();
        Future<String> future = Async.schedule(new HelloCallable())
                .onSuccess(listener)
                .onError(listener)
                .start();
        Thread.sleep(200);
        Robolectric.flushForegroundThreadScheduler();
        assertThat("expect 'Hello'", future.fetch(), is("Hello"));
        assertThat("expect 'Hello'", listener.getResult(), is("Hello"));
    }

    @Test
    public void schedule_returnError_expectError() throws Exception {
        ResultListener<String, Exception> listener = new ResultListener<>();
        Future<String> future = Async.schedule(new ErrorCallable())
                .onSuccess(listener)
                .onError(listener)
                .start();
        Thread.sleep(200);
        Robolectric.flushForegroundThreadScheduler();
        try {
            future.fetch();
            fail("expect UnsupportedOperationException");
        } catch (UnsupportedOperationException ex) {
            assertThat("expect 'Hello'", listener.getError(), instanceOf(UnsupportedOperationException.class));
        }
    }

    @Test(expected = TimeoutException.class)
    public void schedule_cancelListener_expectNoCallback() throws Exception {
        ResultListener<String, Exception> listener = new ResultListener<>();
        Future<String> future = Async.schedule(new HelloCallable())
                .onSuccess(listener)
                .onError(listener)
                .start();
        future.cancel(true);
        Thread.sleep(200);
        Robolectric.flushForegroundThreadScheduler();
        listener.getResult(); // throws exception
    }

    @Test
    public void start_returnResult_expectResult() throws Exception {
        Future<String> future = Async.start(new HelloCallable());
        assertThat("expect 'Hello'", future.fetch(), is("Hello"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void start_returnError_expectError() throws Exception {
        Future<String> future = Async.start(new ErrorCallable());
        future.fetch(); // expect exception
    }

    static class ResultListener<DATA, ERROR extends Exception> implements ResponseListener<DATA>, ErrorListener<ERROR> {
        CountDownLatch mLatch = new CountDownLatch(1);
        ERROR mError;
        DATA mResult;

        @Override
        public void onResult(DATA result) {
            mResult = result;
            mLatch.countDown();
        }

        @Override
        public void onError(ERROR error) {
            mError = error;
            mLatch.countDown();
        }

        public DATA getResult() throws Exception {
            if (!mLatch.await(500, TimeUnit.MILLISECONDS)) throw new TimeoutException();
            if (mError != null) throw mError;
            return mResult;
        }

        public Exception getError() throws InterruptedException, TimeoutException {
            if (!mLatch.await(500, TimeUnit.MILLISECONDS)) throw new TimeoutException();
            if (mError != null) return mError;
            throw new RuntimeException("Expected error, got result");
        }
    }

    static class HelloCallable implements Callable<String> {
        @Override
        public String call() throws Exception {
            return "Hello";
        }
    }

    static class ErrorCallable implements Callable<String> {
        @Override
        public String call() throws Exception {
            throw new UnsupportedOperationException();
        }
    }
}
