package com.truecaller.emarc.parser;

import com.truecaller.emarc.BuildConfig;
import com.truecaller.emarc.common.async.Async;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Unit test for parser service
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(RobolectricGradleTestRunner.class)
public class ParserServiceTest extends TestCase {

    /** Parser Service Implementation for testing **/
    private final ParserService mSubject = new ParserServiceImpl();
    /** Dummy text data for testing **/
    private static final String mData = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ";

    @Before
    public void setUp() {
        Async.DISABLE_EXECUTOR_DELEGATION = true;
        Async.DISABLE_UI_THREAD_CHECK = true;
    }

    @Test
    public void parserService_tenthCharacterValidData_isCorrect() throws Exception {
        char tenthCharacter = mSubject.getTenthCharacter(mData);

        assertThat(tenthCharacter, not('x'));
        assertThat("Tenth character should be 'u'", tenthCharacter, is('u'));
    }

    @Test
    public void parserService_tenthCharacterInvalidLength_isCorrect() throws Exception {
        char tenthCharacter = mSubject.getTenthCharacter("hello");

        assertThat("Character should be null char", tenthCharacter, is('\0'));
    }

    @Test
    public void parserService_tenthCharacterNullData_isCorrect() throws Exception {
        char tenthCharacter = mSubject.getTenthCharacter(null);

        assertThat("Character should be null char", tenthCharacter, is('\0'));
    }

    @Test
    public void parserService_everyTenthCharacterValidData_isCorrect() throws Exception {
        List<Character> everyTenth = mSubject.getMultipleTenthCharacters(mData);

        assertNotNull(everyTenth);
        assertThat("list should not be empty", everyTenth.isEmpty(), is(false));
        assertThat("Character should be null u", everyTenth.get(0), is(mData.charAt(9)));
        assertThat("Character should be null l", everyTenth.get(1), is(mData.charAt(19)));
        assertThat("Character should be null e", everyTenth.get(2), is(mData.charAt(29)));
    }

    @Test
    public void parserService_everyTenthCharacterInvalidData_isCorrect() throws Exception {
        List<Character> everyTenth = mSubject.getMultipleTenthCharacters("hello");
        assertNotNull(everyTenth);
        assertThat("list should be empty", everyTenth.isEmpty(), is(true));
    }

    @Test
    public void parserService_everyTenthCharacterNullData_isCorrect() throws Exception {
        List<Character> everyTenth = mSubject.getMultipleTenthCharacters(null);
        assertNotNull(everyTenth);
        assertThat("list should be empty", everyTenth.isEmpty(), is(true));
    }

    @Test
    public void parserService_getWordCountValidData_isCorrect() throws Exception {
        Map<String, Integer> words = mSubject.getWordCount(mData);
        assertNotNull(words);
        assertThat("list should not be empty", words.isEmpty(), is(false));
        assertThat("list size should (12)", words.size(), is(12));
        assertThat("typesetting word frequency should be 3", words.get("typesetting"), is(3));
    }

    @Test
    public void parserService_getWordCountInvalidData_isCorrect() throws Exception {
        Map<String, Integer> words = mSubject.getWordCount("");
        assertNotNull(words);
        assertThat("list should be empty", words.isEmpty(), is(true));
    }

    @Test
    public void parserService_getWordCountNullData_isCorrect() throws Exception {
        Map<String, Integer> words = mSubject.getWordCount(null);
        assertNotNull(words);
        assertThat("list should be empty", words.isEmpty(), is(true));
    }
}