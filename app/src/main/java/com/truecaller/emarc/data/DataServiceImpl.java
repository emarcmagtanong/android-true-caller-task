package com.truecaller.emarc.data;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.truecaller.emarc.common.async.Async;

import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

/**
 * Network implementation using {@link Volley} of {@link DataService}
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class DataServiceImpl implements DataService {

    /** Volley request queue **/
    @Inject RequestQueue mRequestQueue;

    @Inject
    DataServiceImpl() {
    }

    @Override
    public String getHttpData() throws ExecutionException, InterruptedException {
        Async.checkOnNonUiThread();
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(END_POINT, future, future);
        mRequestQueue.add(request);
        // sanitize string from white spaces
        return future.get().replaceAll("\\s+", " ");
    }
}
