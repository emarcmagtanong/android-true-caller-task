package com.truecaller.emarc.data;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Data module for providing network related dependencies
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
@Module
public final class DataModule {

    @Provides
    @Singleton
    public RequestQueue providesRequestQueue(Application applicationContext) {
        RequestQueue queue = Volley.newRequestQueue(applicationContext);
        queue.start();
        return queue;
    }

    @Provides
    @Singleton
    public DataService providesDataService(DataServiceImpl impl) {
        return impl;
    }
}
