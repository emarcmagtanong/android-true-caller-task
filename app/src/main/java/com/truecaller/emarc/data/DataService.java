package com.truecaller.emarc.data;

import com.truecaller.emarc.common.async.BlockingCallOnMainThreadException;

import java.util.concurrent.ExecutionException;

/**
 * Data service interface
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public interface DataService {

    String END_POINT = "https://www.truecaller.com/privacy-policy";

    /**
     * Fetches raw payload from {@link DataService#END_POINT}.
     * NOTE: This is a blocking call and should be called on a worker thread.
     *
     * @return string payload
     * @throws BlockingCallOnMainThreadException throws exception if call on the main thread
     * @throws ExecutionException                throws thread exception
     * @throws InterruptedException              throws thread exception
     */
    String getHttpData() throws BlockingCallOnMainThreadException, ExecutionException, InterruptedException;
}
