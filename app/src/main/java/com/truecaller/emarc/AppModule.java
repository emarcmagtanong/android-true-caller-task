package com.truecaller.emarc;

import android.app.Application;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Application module for dependency injection.
 *
 * @author by Emarc Magtanong on 11/15/15.
 */
@Module
public final class AppModule {
    /** Application context **/
    private final TrueCallerApp mApplication;

    public AppModule(TrueCallerApp applicationContext) {
        mApplication = applicationContext;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Resources providesResources() {
        return mApplication.getResources();
    }
}
