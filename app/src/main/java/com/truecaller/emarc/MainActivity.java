package com.truecaller.emarc;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.truecaller.emarc.common.async.Async;
import com.truecaller.emarc.common.async.Cancelable;
import com.truecaller.emarc.common.base.DaggerBaseActivity;
import com.truecaller.emarc.common.widget.LinearLayoutManager;
import com.truecaller.emarc.data.DataService;
import com.truecaller.emarc.parser.ParserService;
import com.truecaller.emarc.tenthcharacter.TenthCharacterAdapter;
import com.truecaller.emarc.wordcount.WordCountRecyclerAdapter;
import com.truecaller.emarc.wordcount.WordCountSimpleAdapter;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Main Activity for showing results
 */
public final class MainActivity extends DaggerBaseActivity implements SearchView.OnQueryTextListener{

    /** Log tag **/
    private static final String TAG = MainActivity.class.getSimpleName();
    /** {@link DataService} for getting data from data source **/
    @Inject DataService mDataService;
    /** {@link ParserService} for parsing {@link DataService#getHttpData()} **/
    @Inject ParserService mParserService;
    /** {@link Resources} for Android resources **/
    @Inject Resources mResources;

    /** View bindings **/
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.tenth_character_result)
    TextView mTenthCharacterResult;
    @Bind(R.id.tenth_character_recycler)
    RecyclerView mTenthCharacterRecycler;
    @Bind(R.id.word_count_recycler)
    RecyclerView mWordCountRecycler;
    private SearchView mSearchView;

    /** Async token for data request **/
    private Cancelable mDataRequest = Async.none();
    /** Async token for all tenth character request **/
    private Cancelable mTenthCharRequest = Async.none();
    /** Async token for creating dictionary request **/
    private Cancelable mDictionaryRequest = Async.none();
    /** Blocking progress dialog **/
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        setupUi();
        getData();
    }

    /**
     * Setups views for this activity
     */
    private void setupUi() {
        mTenthCharacterRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mWordCountRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                getData();
                break;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Calls {@link DataService} and gets data from {@link DataService#END_POINT}
     */
    private void getData() {
        if (!mDataRequest.isDone() && !mTenthCharRequest.isDone() && !mDictionaryRequest.isDone()) {
            return;
        }

        mDataRequest = Async.schedule(mDataService::getHttpData)
                .onSuccess(this::onDataServiceSuccess)
                .onError(this::onTaskError)
                .start();
        mProgressDialog = createBlockingProgressDialog("Fetching data...");
        mProgressDialog.show();
    }

    /**
     * Success callback from {@link DataService} request
     *
     * @param data raw payload from {@link DataService}
     */
    private void onDataServiceSuccess(String data) {
        Log.d(TAG, data);
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        char tenthCharacter = mParserService.getTenthCharacter(data);
        mTenthCharacterResult.setText(String.format("Result: [%s]", tenthCharacter));
        mTenthCharacterResult.setVisibility(View.VISIBLE);

        mTenthCharRequest = Async.schedule(() -> mParserService.getMultipleTenthCharacters(data))
                .onSuccess(this::onMultipleTensResult)
                .onError(this::onTaskError)
                .start();

        mDictionaryRequest = Async.schedule(() -> mParserService.getWordCount(data))
                .onSuccess(this::onWordCountResult)
                .onError(this::onTaskError)
                .start();
    }

    /**
     * Catches result from {@link ParserService#getMultipleTenthCharacters(String)}
     *
     * @param result list of character on indices which are multiples of 10
     */
    private void onMultipleTensResult(List<Character> result) {
        mTenthCharacterRecycler.setAdapter(new TenthCharacterAdapter(result));
        mTenthCharacterRecycler.setVisibility(View.VISIBLE);
    }

    /**
     * Catches result from {@link ParserService#getWordCount(String)} (String)}
     *
     * @param result map of word and its frequency
     */
    private void onWordCountResult(Map<String, Integer> result) {
        mWordCountRecycler.setAdapter(new WordCountRecyclerAdapter((TreeMap<String, Integer>) result));
        mSearchView.setSuggestionsAdapter(new WordCountSimpleAdapter(this, result));
        mWordCountRecycler.setVisibility(View.VISIBLE);
    }

    /**
     * Catches errors from {@link Async} request
     *
     * @param exception thrown exception
     */
    private void onTaskError(Exception exception) {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        toastShort("Opps something went wrong...");
        exception.printStackTrace();
    }

    @Override
    protected void inject() {
        mAppComponent.inject(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDataRequest.cancel();
        mTenthCharRequest.cancel();
        mDictionaryRequest.cancel();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
