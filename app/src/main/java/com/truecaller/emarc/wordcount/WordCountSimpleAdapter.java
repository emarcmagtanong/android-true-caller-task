package com.truecaller.emarc.wordcount;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;

import com.truecaller.emarc.R;

import java.util.Map;

/**
 * Cursor adapter for search suggestions for the search view.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class WordCountSimpleAdapter extends SimpleCursorAdapter {

    /** Dummy cursor fields **/
    private static final String[] mFields = {"_id", "word", "count"};
    /** Visible column data **/
    private static final String[] mVisible = {"word", "count"};
    /** Text views to bind to **/
    private static final int[] mViews = {R.id.word_text_view, R.id.word_count_text_view};
    /** Cursor data **/
    private final Map<String, Integer> mData;

    public WordCountSimpleAdapter(Context context, Map<String, Integer> data) {
        super(context, R.layout.word_count_item, null, mVisible, mViews, 0);
        mData = data;
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        return new WordCountCursor(constraint, mData, mFields);
    }
}
