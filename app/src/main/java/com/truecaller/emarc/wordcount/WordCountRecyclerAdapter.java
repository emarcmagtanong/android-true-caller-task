package com.truecaller.emarc.wordcount;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.truecaller.emarc.R;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Adapter for every tenth character results.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class WordCountRecyclerAdapter extends RecyclerView.Adapter<WordCountRecyclerAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        /** View bindings **/
        @Bind(R.id.word_text_view)
        TextView mWordTextView;
        @Bind(R.id.word_count_text_view)
        TextView mWordCountTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /** Map of word and frequency **/
    private final TreeMap<String, Integer> mData;
    /** List for data iteration **/
    private final List<String> mKeyArray = new ArrayList<>();

    public WordCountRecyclerAdapter(@NonNull TreeMap<String, Integer> data) {
        mData = data;
        mKeyArray.addAll(data.keySet());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.word_count_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mWordTextView.setText(mKeyArray.get(position));
        holder.mWordCountTextView.setText(String.format("[%s]", mData.get(mKeyArray.get(position))));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
