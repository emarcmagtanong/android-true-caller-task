package com.truecaller.emarc.wordcount;

import android.database.AbstractCursor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Cursor for word count search suggestions.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class WordCountCursor extends AbstractCursor {

    /** Dummy column names **/
    private final String[] mColumnNames;
    /** List of keys **/
    private final List<String> mKeys;
    /** Data **/
    private final Map<String, Integer> mData;

    public WordCountCursor(CharSequence constraint, Map<String, Integer> wordCountResults, String[] columnNames) {
        mKeys = Optional.of(StreamSupport.stream(wordCountResults.keySet())
                .filter(key -> key != null &&
                        constraint != null &&
                        key.toLowerCase().contains(constraint.toString().toLowerCase()))
                .collect(Collectors.toList()))
                .orElseGet(ArrayList::new);
        mData = wordCountResults;
        mColumnNames = columnNames;
    }

    @Override
    public int getCount() {
        return mKeys.size();
    }

    @Override
    public String[] getColumnNames() {
        return mColumnNames;
    }

    @Override
    public String getString(int column) {
        switch (column) {
            case 1:
                return mKeys.get(getPosition());
            case 2:
                return mData.get(mKeys.get(getPosition())).toString();
            default:
                throw new UnsupportedOperationException("unimplemented");
        }
    }

    @Override
    public short getShort(int column) {
        throw new UnsupportedOperationException("unimplemented");
    }

    @Override
    public int getInt(int column) {
        throw new UnsupportedOperationException("unimplemented");
    }

    @Override
    public long getLong(int column) {
        switch (column) {
            case 0:
                return getPosition();
            default:
                throw new UnsupportedOperationException("unimplemented");
        }
    }

    @Override
    public float getFloat(int column) {
        throw new UnsupportedOperationException("unimplemented");
    }

    @Override
    public double getDouble(int column) {
        throw new UnsupportedOperationException("unimplemented");
    }

    @Override
    public boolean isNull(int column) {
        return false;
    }
}
