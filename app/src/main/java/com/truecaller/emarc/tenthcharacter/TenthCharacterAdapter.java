package com.truecaller.emarc.tenthcharacter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.truecaller.emarc.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Adapter for every tenth character results.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class TenthCharacterAdapter extends RecyclerView.Adapter<TenthCharacterAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        /** View bindings **/
        @Bind(R.id.character_text_view)
        TextView mCharacterTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private final List<Character> mData;

    public TenthCharacterAdapter(@NonNull List<Character> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.tenth_character_item, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mCharacterTextView.setText("[" + mData.get(position) + "]");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
