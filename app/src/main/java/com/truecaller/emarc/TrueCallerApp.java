package com.truecaller.emarc;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Application class for TrueCaller
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class TrueCallerApp extends Application {
    /** App main component **/
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDagger();
    }

    private void initializeDagger() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static TrueCallerApp get(@NonNull Context context) {
        return (TrueCallerApp) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
