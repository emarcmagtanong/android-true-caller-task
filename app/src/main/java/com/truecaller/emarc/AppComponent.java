package com.truecaller.emarc;

import com.truecaller.emarc.data.DataModule;
import com.truecaller.emarc.parser.ParserModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Main app component for dependency injection.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                DataModule.class,
                ParserModule.class
        }
)
public interface AppComponent {
    /**
     * Injects {@link MainActivity} dependencies
     *
     * @param activity {@link MainActivity} instance
     */
    void inject(MainActivity activity);
}
