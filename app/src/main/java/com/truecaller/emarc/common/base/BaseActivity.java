package com.truecaller.emarc.common.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Base activity with UI convenience methods
 *
 * @author by Emarc Magtanong on 9/27/15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Starts a new activity and clears the activity stack
     *
     * @param intent activity to launch
     */
    protected void startActivityClearStack(@NonNull Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /**
     * Creates and shows a short toast message
     *
     * @param message toast message
     */
    protected void toastShort(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    /**
     * Creates and shows a short toast message
     *
     * @param stringId string resource ID
     */
    protected void toastShort(@StringRes int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_SHORT).show();
    }

    /**
     * Creates and shows a long toast message
     *
     * @param message toast message
     */
    protected void toastLong(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    /**
     * Creates and shows a long toast message
     *
     * @param stringId string resource ID
     */
    protected void toastLong(@StringRes int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }

    /**
     * Creates an instance of {@link ProgressDialog} that is indeterminate and cannot be cancelled
     *
     * @param message progress dialog message
     * @return a new {@link ProgressDialog} instance
     */
    protected ProgressDialog createBlockingProgressDialog(@NonNull String message) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        return progressDialog;
    }
}
