package com.truecaller.emarc.common.async;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.truecaller.emarc.common.callback.ErrorListener;
import com.truecaller.emarc.common.callback.ResponseListener;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Class for delegating method operations to a worker thread
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public final class Async {
    /** Tag for logging **/
    private final static String LOG_TAG = "Async";
    /** Set this flag in order to disable the check for main/UI thread **/
    public static boolean DISABLE_UI_THREAD_CHECK = false;
    /** Set this flag in order to execute all tasks immediately blocking on the calling thread (ONLY FOR UNIT TESTS) **/
    public static boolean DISABLE_EXECUTOR_DELEGATION = false;
    /** Enables/disables log output **/
    public static boolean DEBUG = false;
    /** Default thread executor **/
    private static ExecutorService sExecutor = Executors.newCachedThreadPool();

    /**
     * Creates a default future that can be cancelled
     *
     * @param <DATA> request data type
     * @return request data
     */
    public static <DATA> Future<DATA> none() {
        AsyncFuture<DATA> future = new AsyncFuture<>();
        future.deliverResult(null);
        return future;
    }

    /**
     * Creates a default future that can be cancelled with a default result
     *
     * @param result default result
     * @param <DATA> request data type
     * @return request data
     */
    public static <DATA> Future<DATA> none(DATA result) {
        AsyncFuture<DATA> future = new AsyncFuture<>();
        future.deliverResult(result);
        return future;
    }

    /**
     * Creates a new async task builder
     *
     * @param callable callable that will be executed on a worker thread
     * @param <DATA>   result data type
     * @return new async future
     */
    public static <DATA> Task<DATA> schedule(Callable<DATA> callable) {
        return new Task<>(callable);
    }

    /**
     * Creates a new async task builder for tasks without result
     *
     * @param callable callable that will be executed on background thread
     * @return new async future
     */
    public static Task<Void> schedule(final CallableNoResult callable) {
        return new Task<>(() -> {
            callable.call();
            return null;
        });
    }

    /**
     * Creates a started new async task without listener, shortcut for {@code schedule(...).start()}
     *
     * @param callable callable that will be executed on background thread
     * @param <DATA>   result data type
     * @return new async future
     */
    public static <DATA> Future<DATA> start(Callable<DATA> callable) {
        return schedule(callable).start();
    }

    /**
     * Creates a started async task without listeners and without a result, shortcut for {@code schedule(...).start()}
     *
     * @param callable callable that will be executed on background thread
     * @return new async future
     */
    public static Future<Void> start(CallableNoResult callable) {
        return schedule(callable).start();
    }

    /**
     * Unwraps the exception in case its encapsulated in an {@link ExecutionException
     *
     * @param exception exception to be unwrapped
     * @return unwrapped exception or e
     */
    static Exception unwrap(Exception exception) {
        if (exception instanceof ExecutionException && exception.getCause() instanceof Exception) {
            return (Exception) exception.getCause();
        }
        return exception;
    }

    /**
     * Checks if operation is done on a background thread
     *
     * @throws BlockingCallOnMainThreadException if invoked on UI thread
     */
    public static void checkOnNonUiThread() throws BlockingCallOnMainThreadException {
        if (isUiThread() && !DISABLE_UI_THREAD_CHECK) throw new BlockingCallOnMainThreadException();
    }

    /**
     * Checks if the operation is done on the main/UI thread
     *
     * @return true if on main/UI thread, false otherwise
     */
    @VisibleForTesting
    public static boolean isUiThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /**
     * A task that executes on a worker thread. This will be executed by {@link Async#sExecutor}
     */
    public static class Task<DATA> {
        /** Tracks the number of operations for logging **/
        static AtomicLong mNumber = new AtomicLong(0);
        /** Operation to execute **/
        final Callable<DATA> mCallable;
        /** Success listener **/
        @Nullable private ResponseListener<DATA> mResponseListener;
        /** Error listener **/
        @Nullable private ErrorListener<Exception> mErrorListener;
        /** Executor to delegate to worker thread **/
        private Executor mExecutor;
        /** Handle to delegate to main/UI thread **/
        private Handler mHandler;
        /** Result start **/
        private AsyncFuture<DATA> mFuture;
        /** Tag name for loggin **/
        private String mTag;
        /** Log timestamp **/
        private long mLogTimestamp = 0L;
        /** Flag whether started or not **/
        private boolean mStarted = false;

        Task(final Callable<DATA> callable) {
            mCallable = callable;
            mExecutor = sExecutor;
            mHandler = new Handler(Looper.getMainLooper());
            mFuture = new AsyncFuture<>();
            mTag = "task_" + mNumber.addAndGet(1L);
        }

        /**
         * Sets success callback listener
         *
         * @param responseListener listener
         * @return this
         */
        public Task<DATA> onSuccess(@Nullable ResponseListener<DATA> responseListener) {
            mResponseListener = responseListener;
            return this;
        }

        /**
         * Sets error callback listener
         *
         * @param errorListener error listener
         * @return this
         */
        public Task<DATA> onError(@Nullable ErrorListener<Exception> errorListener) {
            mErrorListener = errorListener;
            return this;
        }

        /**
         * Specify special executor to use (default is a shared cached thread pool)
         *
         * @param executor executor
         * @return executor
         */
        public Task<DATA> executor(Executor executor) {
            mExecutor = executor;
            return this;
        }

        /**
         * Set name for request
         *
         * @param name name
         * @return return builder
         */
        public Task<DATA> tag(String name) {
            mTag = name;
            return this;
        }

        /**
         * Start execution
         */
        public synchronized Future<DATA> start() {
            if (mStarted) throw new IllegalStateException("Already started");
            mStarted = true;

            runOnBackground(() -> {
                mLogTimestamp = System.currentTimeMillis();
                try {
                    // execute
                    final DATA result = mCallable.call();
                    // forward result to start
                    if (DEBUG) {
                        log("result", String.valueOf(result));
                    }
                    // deliver result
                    if (!mFuture.isCancelled()) {
                        // if we have a listener -> deliver on UI thread
                        if (mResponseListener != null) {
                            runOnUI(() -> {
                                if (!mFuture.isCancelled()) {
                                    mFuture.deliverResult(result);
                                    mResponseListener.onResult(result);
                                }
                            });
                        } else {
                            // otherwise, deliver on worker thread
                            mFuture.deliverResult(result);
                        }
                    }
                } catch (final Exception ex) {
                    // forward result to start
                    final Exception exception = Async.unwrap(ex);
                    if (DEBUG) {
                        log("error", exception.toString());
                    }
                    // if not cancelled
                    if (!mFuture.isCancelled()) {
                        // if we have a listener -> deliver on UI thread
                        if (mErrorListener != null) {
                            runOnUI(() -> {
                                if (!mFuture.isCancelled()) {
                                    mFuture.deliverError(exception);
                                    mErrorListener.onError(exception);
                                }
                            });
                        } else {
                            // otherwise, deliver on worker thread
                            mFuture.deliverError(exception);
                        }
                    }
                }
            });
            return mFuture;
        }

        /**
         * Writes log entry
         *
         * @param action current action
         * @param result result object or null
         */
        private void log(String action, @Nullable String result) {
            result = result != null ? (" -> " + result) : "";
            Log.d(LOG_TAG, String.format("[%1$14s] %2$5sms %3$s%4$s", mTag, ("+" + String.valueOf(System.currentTimeMillis() - mLogTimestamp)), action, result));
            mLogTimestamp = System.currentTimeMillis();
        }

        /**
         * Runs task on a background thread
         *
         * @param runnable runnable to execute
         */
        private void runOnBackground(Runnable runnable) {
            if (DISABLE_EXECUTOR_DELEGATION) {
                runnable.run();
            } else {
                mExecutor.execute(runnable);
            }
        }

        /**
         * Runs task on main/UI thread
         *
         * @param runnable runnable to execute
         */
        private void runOnUI(Runnable runnable) {
            if (DISABLE_EXECUTOR_DELEGATION) {
                runnable.run();
            } else {
                mHandler.post(runnable);
            }
        }
    }
}
