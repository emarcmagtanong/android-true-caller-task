package com.truecaller.emarc.common.async;

import android.support.annotation.Nullable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * Base future implementation for blocking asynchronous requests
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public class AsyncFuture<DATA> implements Future<DATA> {
    /** Latch to block until response has been delivered **/
    final private CountDownLatch mLatch;
    /** Abort status of the request **/
    private boolean mAborted = false;
    /** Result in case of success **/
    private DATA mResult;
    /** Error in case of failure **/
    private Throwable mErrorResult;

    public AsyncFuture() {
        mLatch = new CountDownLatch(1);
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        if (mLatch.getCount() == 0) {
            return false;
        }
        mAborted = true;
        while (mLatch.getCount() > 0) {
            mLatch.countDown();
        }
        return true;
    }

    @Override
    public boolean cancel() {
        return cancel(true);
    }

    @Override
    public final boolean isCancelled() {
        return mAborted;
    }

    @Override
    public final boolean isDone() {
        return mLatch.getCount() == 0;
    }

    /**
     * Delivers the result
     *
     * @param result result to deliver
     */
    protected void deliverResult(@Nullable DATA result) {
        mResult = result;
        mLatch.countDown();
    }

    /**
     * Delivers the error
     *
     * @param error error to deliver
     */
    protected void deliverError(Throwable error) {
        mErrorResult = error;
        mLatch.countDown();
    }

    @Override
    public DATA get() throws InterruptedException, ExecutionException {
        mLatch.await();

        // deliver result
        if (mAborted) throw new InterruptedException();
        if (mErrorResult != null) throw new ExecutionException(mErrorResult);
        return mResult;
    }

    @Override
    public DATA get(long timeout, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        boolean finished = mLatch.await(timeout, timeUnit);

        // deliver result
        if (!finished) throw new TimeoutException();
        if (mAborted) throw new InterruptedException();
        if (mErrorResult != null) throw new ExecutionException(mErrorResult);
        return mResult;
    }

    @Override
    public DATA fetch() throws Exception {
        try {
            return get();
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof Exception) throw (Exception) ex.getCause();
            if (ex.getCause() instanceof Error) throw (Error) ex.getCause();
            throw new RuntimeException(ex.getCause());
        }
    }

    @Override
    public DATA fetch(long timeout, TimeUnit timeUnit) throws Exception {
        try {
            return get(timeout, timeUnit);
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof Exception) throw (Exception) ex.getCause();
            if (ex.getCause() instanceof Error) throw (Error) ex.getCause();
            throw new RuntimeException(ex.getCause());
        }
    }
}
