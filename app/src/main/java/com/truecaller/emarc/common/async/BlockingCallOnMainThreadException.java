package com.truecaller.emarc.common.async;

/**
 * Exception thrown when calling a blocking method/operation on the main thread
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public final class BlockingCallOnMainThreadException extends RuntimeException {
}