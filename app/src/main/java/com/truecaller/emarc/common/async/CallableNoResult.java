package com.truecaller.emarc.common.async;

import java.util.concurrent.Callable;

/**
 * Same as {@link Callable} but has no return
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public interface CallableNoResult {
    /**
     * See {@link Callable#call()}
     *
     * @throws Exception exception that occurred during operation
     */
    void call() throws Exception;
}
