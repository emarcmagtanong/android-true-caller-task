package com.truecaller.emarc.common.callback;

/**
 * Generic callback when an error occurs during a request
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public interface ErrorListener<ERROR> {
    /**
     * Callback for error
     *
     * @param error error that occurred performing the request
     */
    void onError(ERROR error);
}
