package com.truecaller.emarc.common.async;

/**
 * Interface for cancelable requests
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public interface Cancelable {
    /**
     * Checks if task is done or cancelled
     *
     * @return true if done, false otherwise
     */
    boolean isDone();

    /**
     * Cancels task and all callbacks attached to the request
     *
     * @return false if task was already done, true if cancelled
     */
    boolean cancel();

    /**
     * Check if task has been cancelled
     *
     * @return true if cancelled, false otherwise
     */
    boolean isCancelled();
}
