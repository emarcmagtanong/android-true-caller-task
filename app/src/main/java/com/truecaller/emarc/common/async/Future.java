package com.truecaller.emarc.common.async;

import java.util.concurrent.TimeUnit;

/**
 * Interface for {@link java.util.concurrent.Future} implementation
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public interface Future<DATA> extends java.util.concurrent.Future<DATA>, Cancelable {
    /**
     * Same as {@link #get()} but {@code ExecutionException} are unwrapped in original exception
     *
     * @see {@link #get()}
     */
    DATA fetch() throws Exception;

    /**
     * Same as {@link #get(long, TimeUnit)} but {@code ExecutionException} are unwrapped in original exception
     *
     * @see {@link #get(long, TimeUnit)}
     */
    DATA fetch(long timeout, TimeUnit timeUnit) throws Exception;

    /**
     * Same as {@code #cancel(true)}.
     *
     * @see {@link #cancel(boolean)}
     */
    boolean cancel();
}