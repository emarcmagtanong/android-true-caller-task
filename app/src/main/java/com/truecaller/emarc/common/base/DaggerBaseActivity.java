package com.truecaller.emarc.common.base;

import android.os.Bundle;

import com.truecaller.emarc.AppComponent;
import com.truecaller.emarc.TrueCallerApp;


/**
 * Base activity with dependency injection to the Global Object Graph
 *
 * @author by Emarc Magtanong on 9/24/15.
 */
public abstract class DaggerBaseActivity extends BaseActivity {

    /** Component for exposing the Global Object Graph **/
    protected AppComponent mAppComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent();
        inject();
    }

    /**
     * Initializes dagger App component
     */
    protected void getApplicationComponent() {
        mAppComponent = TrueCallerApp.get(this).getAppComponent();
    }

    /**
     * Inject Activity dependencies
     */
    protected abstract void inject();
}
