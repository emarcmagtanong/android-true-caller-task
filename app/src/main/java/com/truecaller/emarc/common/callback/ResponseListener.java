package com.truecaller.emarc.common.callback;

/**
 * Generic callback for successful requests
 *
 * @author by Emarc Magtanong on 9/23/15.
 */
public interface ResponseListener<DATA> {
    /**
     * Callback for success
     *
     * @param data result of the request
     */
    void onResult(DATA data);
}
