package com.truecaller.emarc.parser;

import android.support.annotation.NonNull;

import com.truecaller.emarc.common.async.BlockingCallOnMainThreadException;

import java.util.List;
import java.util.TreeMap;

/**
 * Parsing service for getting required output specified on the assignment document.
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public interface ParserService {
    /** Required index of interest **/
    int INDEX = 10;

    /**
     * Gets the 10th character of the string.
     *
     * @param data string to parse
     * @return the 10th character of a given string
     */
    char getTenthCharacter(@NonNull String data);

    /**
     * Gets all characters on indices that are multiples of 10.
     * Note: Potentially a long running task, call on a worker thread.
     *
     * @param data string to parse
     * @return list of characters with indices are multiples of 10
     * @throws BlockingCallOnMainThreadException throws exception if called on the main thread
     */
    List<Character> getMultipleTenthCharacters(@NonNull String data) throws BlockingCallOnMainThreadException;

    /**
     * Creates a map with tokens as keys and frequency as values.
     * Note: Potentially a long running task, call on a worker thread.
     *
     * @param data string to parse
     * @return a map of the tokenize data
     * @throws BlockingCallOnMainThreadException throws exception if called on the main thread
     */
    TreeMap<String, Integer> getWordCount(@NonNull String data) throws BlockingCallOnMainThreadException;
}
