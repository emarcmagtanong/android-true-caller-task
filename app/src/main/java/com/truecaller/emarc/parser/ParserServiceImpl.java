package com.truecaller.emarc.parser;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.truecaller.emarc.common.async.Async;
import com.truecaller.emarc.common.async.BlockingCallOnMainThreadException;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.inject.Inject;

import java8.util.stream.Collectors;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

/**
 * Implementation of {@link ParserService} using {@link RefStreams} (StreamSupport).
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
public final class ParserServiceImpl implements ParserService {

    @Inject
    @VisibleForTesting
    public ParserServiceImpl() {
    }

    @Override
    public char getTenthCharacter(@NonNull String data) {
        if (data == null || data.length() < INDEX) return '\0';
        return data.charAt(INDEX - 1);
    }

    @Override
    public List<Character> getMultipleTenthCharacters(@NonNull String data) throws BlockingCallOnMainThreadException {
        Async.checkOnNonUiThread();

        if (data == null || data.length() < INDEX) return new ArrayList<>();

        return IntStreams.range(0, data.length())
                .filter(index -> index % INDEX == (INDEX - 1))
                .mapToObj(data::charAt)
                .collect(Collectors.toList());
    }

    @Override
    public TreeMap<String, Integer> getWordCount(@NonNull String data) throws BlockingCallOnMainThreadException {
        Async.checkOnNonUiThread();

        if (data == null || data.length() == 0) return new TreeMap<>();

        return RefStreams.of(data.split("\\s+"))
                .map(String::new)
                .collect(Collectors.toMap(key -> key,
                        value -> 1,
                        (oldValue, newValue) -> ++oldValue,
                        TreeMap::new));
    }
}
