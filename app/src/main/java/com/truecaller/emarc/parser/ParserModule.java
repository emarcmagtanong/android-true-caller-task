package com.truecaller.emarc.parser;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Parser module for providing {@link ParserService}
 *
 * @author by Emarc Magtanong on 12/3/15.
 */
@Module
public final class ParserModule {
    @Provides
    @Singleton
    ParserService providesParserService(ParserServiceImpl impl) {
        return impl;
    }
}
